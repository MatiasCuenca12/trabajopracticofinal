#include <stdio.h>
#include "headers.h"

/*
 * Ejecuta el programa
 */
void runMenu(){
    ListaEstudiantes le = crearListaEstudiantes();
    ListaMaterias lm = crearListaMaterias();
    Estudiante *e;
    Estudiante e2;
    Materia materia;
    Materia *m;
    int omp, ome, obe, omm, omi, omrm, omest;

    do{
        printf("\n-- MENU PRINCIPAL --\n");
        printf("1 - Estudiantes\n");
        printf("2 - Materias\n");
        printf("3 - Inscripciones\n");
        printf("4 - Estadisticas\n");
        printf("5 - Test\n");
        printf("6 - Salir\n");
        printf(">>Ingrese una opcion: ");
        scanf("%d", &omp);

        switch (omp) {
            case 1:
                do {
                    printf("\n-- MENU ESTUDIANTES --\n");
                    printf("1 - Agregar estudiante\n");
                    printf("2 - Agregar varios estudiantes\n");
                    printf("3 - Buscar estudiante\n");
                    printf("4 - Listar estudiantes con paginado\n");
                    printf("5 - Eliminar estudiante\n");
                    printf("6 - Volver al menu principal\n");
                    printf(">>Ingrese una opcion: ");
                    scanf("%d", &ome);

                    switch (ome) {
                        case 1:
                            e2 = crearEstudiante();
                            if(estudianteRegistrado(le, e2)){
                                printf ("[ YA EXISTE ESTUDIANTE CON ESE DNI/LEGAJO ]\n");
                            }else{
                                if (datosCorrectosEstudiante(&e2)) {
                                    agregarEstudiante(&le, &le.head, e2);
                                    printf("[ ESTUDIANTE AGREGADO ]\n");
                                } else {
                                    printf("[ DATOS ERRONEOS ]\n");
                                }
                            }
                            break;
                        case 2:
                            agregarVariosEstudiantes(&le, &le.head);
                            break;
                        case 3:
                            printf("\n-- MENU BUSQUEDA ESTUDIANTES --\n");
                            printf("1 - Buscar por nombre\n");
                            printf("2 - Buscar por rango de edad\n");
                            printf("3 - Volver al menu de estudiantes\n");
                            printf(">>Ingrese una opcion: ");
                            scanf("%d", &obe);

                            switch (obe) {
                                case 1:
                                    if(hayEstudiantesCargados(le)){
                                        e = buscarEstudiantePorNombre(&le.head);
                                        if(e != NULL) {
                                            printf("ESTUDIANTE ENCONTRADO:\nNombre: %s  Legajo: %d\n", e->nombre,e->legajo);
                                        }
                                    }else{
                                        printf("[ NO HAY ESTUDIANTES CARGADOS ]\n");
                                    }
                                    break;
                                case 2:
                                    if(hayEstudiantesCargados(le)) {
                                        buscarEstudiantesPorRangoDeEdad(&le.head);
                                    }else{
                                        printf("[ NO HAY ESTUDIANTES CARGADOS ]\n");
                                    }
                                    break;
                                case 3:
                                    printf("[ VOLVIENDO AL MENU ESTUDIANTES... ]\n");
                                    break;
                                default:
                                    printf("[ OPCION INCORRECTA ]\n");
                                    break;
                            }
                            break;
                        case 4:
                            if(hayEstudiantesCargados(le)){
                                listarEstudiantesPaginado(&le, &le.head, 1, 1);
                            }else{
                                printf("[ NO HAY ESTUDIANTES CARGADOS ]\n");
                            }
                            break;
                        case 5:
                            if(hayEstudiantesCargados(le)) {
                                listarEstudiantes(&le, &le.head);
                                eliminarEstudianteDeLaLista(&le, &le.head);
                            }else{
                                printf("[ NO HAY ESTUDIANTES CARGADOS ]\n");
                            }
                            break;
                        case 6:
                            printf("[ VOLVIENDO AL MENU PRINCIPAL... ]\n");
                            break;
                        default:
                            printf("[ OPCION INCORRECTA ]\n");
                            break;
                    }
                } while (ome != 6);
                break;
            case 2:
                do {
                    printf("\n-- MENU MATERIAS --\n");
                    printf("1 - Agregar materia\n");
                    printf("2 - Agregar varias materias\n");
                    printf("3 - Buscar materia\n");
                    printf("4 - Listar materias con paginado\n");
                    printf("5 - Eliminar una materia\n");
                    printf("6 - Volver al menu principal\n");
                    printf("Ingrese una opcion: ");
                    scanf("%d", &omm);
                    switch (omm) {
                        case 1:
                            materia = crearMateria();
                            if (datosCorrectosMaterias(materia)) {
                                if(materiaCargada(&lm, materia)){
                                    printf("[ ESTA MATERIA YA ESTA CARGADA ]\n");
                                }else{
                                    agregarMateria(&lm, &lm.head, materia);
                                    printf("[ MATERIA AGREGADA ]\n");
                                }
                            }else{
                                printf("[ DATOS ERRONEOS ]\n");
                            }
                            break;
                        case 2:
                            agregarVariasMaterias(&lm, &lm.head);
                            break;
                        case 3:
                            if(hayMateriasCargadas(lm)){

                                m = buscarMateriaPorNombre(&lm.head);
                                if(m != NULL) {
                                    printf("[ Nombre: %s  Codigo: %d ]\n", m->nombre,m->codigo);
                                }

                            }else{
                                printf("[ NO HAY MATERIAS CARGADAS ]\n");
                            }
                            break;
                        case 4:
                            if(hayMateriasCargadas(lm)) {
                                listarMateriasPaginado(&lm, &lm.head);
                            }else{
                                printf("[ NO HAY MATERIAS CARGADAS ]\n");
                            }
                            break;
                        case 5:
                            if(hayMateriasCargadas(lm)) {
                                listarMaterias(&lm, &lm.head);
                                eliminarMateria(&lm);
                            }else{
                                printf("[ NO HAY MATERIAS CARGADAS ]\n");
                            }
                            break;
                        case 6:
                            printf("[ VOLVIENDO AL MENU PRINCIPAL... ]\n");
                            break;
                        default:
                            printf("[ OPCION INCORRECTA ]\n");
                            break;
                    }
                } while (omm != 6);
                break;
            case 3:
                do {
                    printf("\n-- MENU INSCRIPCIONES --\n");
                    printf("1 - Inscripcion a una materia\n");
                    printf("2 - Rendir materia\n");
                    printf("3 - Darse de baja de una materia\n");
                    printf("4 - Volver al menu principal\n");
                    printf(">>Ingrese una opcion: ");
                    scanf("%d", &omi);

                    switch (omi) {
                        case 1:
                            if (hayEstudiantesCargados(le) && hayMateriasCargadas(lm)) {
                                listarEstudiantes(&le, &le.head);
                                e = buscarEstudiantePorNombre(&le.head);
                                printf("[ MATERIAS DISPONIBLES: %d ]\n", lm.cantMaterias);
                                listarMaterias(&lm, &lm.head);
                                m = buscarMateriaPorNombre(&lm.head);
                                if (m != NULL & e != NULL) {
                                    anotarseEnMateria(m, e);
                                    printf("[ INSCRIPCION EXITOSA ]\n");
                                } else {
                                    printf("[ DATOS INCORRECTOS, INTENTELO NUEVAMENTE ]\n");
                                }
                            } else {
                                printf("[ NO HAY ESTUDIANTES O MATERIAS CARGAD@S ]\n");
                            }
                            break;
                        case 2:
                            if (hayEstudiantesCargados(le) && hayMateriasCargadas(lm)) {
                                printf("\n-- MENU RENDIR MATERIA\n");
                                printf("1 - Ingresar nota\n");
                                printf("2 - Generar nota aleatoria\n");
                                printf("3 - Volver al menu principal\n");
                                printf(">>Ingrese su opcion: ");
                                scanf("%d", &omrm);
                                switch (omrm) {
                                    case 1:
                                        asignarNota(&lm, &le, 1);
                                        break;
                                    case 2:
                                        asignarNota(&lm, &le, 0);
                                        break;
                                    case 3:
                                        printf("[ VOLVIENDO AL MENU INSCRIPCIONES... ]\n");
                                        break;
                                    default:
                                        printf("[ OPCION INCORRECTA ]\n");
                                        break;
                                }
                            } else {
                                printf("[ NO HAY ESTUDIANTES O MATERIAS CARGAD@S ]\n");
                            }
                            break;
                        case 3:
                            if (hayEstudiantesCargados(le) && hayMateriasCargadas(lm)) {
                                darseBajaMateria(&le);
                            } else {
                                printf("[ NO HAY ESTUDIANTES O MATERIAS CARGAD@S ]\n");
                            }
                            break;
                        case 4:
                            printf("[ VOLVIENDO AL MENU PRINCIPAL... ]\n");
                            break;
                        default:
                            printf("[ OPCION INCORRECTA ]\n");
                            break;
                    }
                }while(omi != 4);
                break;
            case 4:
                do {
                    printf("\n-- MENU ESTADISTICAS --\n");
                    printf("1 - Ver nota promedio de un estudiante\n");
                    printf("2 - Ver alumno con mejor promedio\n");
                    printf("3 - Ver materias en curso de un estudiante\n");
                    printf("4 - Ver estudiantes inscriptos en una materia\n");
                    printf("5 - Volver al menu principal\n");
                    printf(">>Ingrese una opcion: ");
                    scanf("%d", &omest);
                    switch (omest) {
                        case 1:
                            if (hayEstudiantesCargados(le)) {
                                listarEstudiantes(&le, &le.head);
                                verPromedioBuscandoEstudiante(&le);
                            } else {
                                printf("[ NO HAY ESTUDIANTES CARGADOS ]\n");
                            }
                            break;
                        case 2:
                            if (hayEstudiantesCargados(le)) {
                                mejorPromedio(le);
                            } else {
                                printf("[ NO HAY ESTUDIANTES CARGADOS ]\n");
                            }
                            break;
                        case 3:
                            if (hayEstudiantesCargados(le)) {
                                listarEstudiantes(&le, &le.head);
                                listarMateriasEstudiante(&le);
                            } else {
                                printf("[ NO HAY ESTUDIANTES CARGADOS ]\n");
                            }
                            break;
                        case 4:
                            if (hayEstudiantesCargados(le) && hayMateriasCargadas(lm)) {
                                printf("[ MATERIAS DISPONIBLES ]\n");
                                listarMaterias(&lm, &lm.head);
                                estudiantesCursandoMateria(&lm, &le);
                            } else {
                                printf("[ NO HAY MATERIAS O ESTUDIANTES CARGAD@S ]\n");
                            }
                            break;
                        case 5:
                            printf("[ VOLVIENDO AL MENU PRINCIPAL... ]\n");
                            break;
                        default:
                            printf("[ OPCION INCORRECTA ]\n");
                            break;
                    }
                }while(omest != 5);
                break;
            case 5: {
                int opcionPrueba = 0;
                do {
                    printf("\n-- MENU DE PRUEBAS --\n");
                    printf("1 - Test agregar Alumno.\n");
                    printf("2 - Test agregar Varios Alumnos.\n");
                    printf("3 - Test buscar Alumno Por Nombre.\n");
                    printf("4 - Test eliminar Estudiante De La Lista.\n");
                    printf("5 - Test crear Materia.\n");
                    printf("6 - Test agregar MateriaALista.\n");
                    printf("7 - Test agregar Varias Materia A Lista.\n");
                    printf("8 - Volver al menu principal.\n");
                    printf(">>Ingrese una opcion: ");
                    scanf("%d", &opcionPrueba);

                    switch (opcionPrueba) {
                        case 1:
                            test_agregarAlumno();
                            printf("[ PRUEBA COMPLETADA ]\n");
                            break;
                        case 2:
                            test_agregarVariosAlumnos();
                            printf("[ PRUEBA COMPLETADA ]\n");
                            break;
                        case 3:
                            test_buscarAlumnoPorNombre();
                            printf("[ PRUEBA COMPLETADA ]\n");
                            break;
                        case 4:
                            test_eliminarEstudianteDeLaLista();
                            printf("[ PRUEBA COMPLETADA ]\n");
                            break;
                        case 5:
                            test_crearMateria();
                            printf("[ PRUEBA COMPLETADA ]\n");
                            break;
                        case 6:
                            test_agregarMateriaALista();
                            printf("[ PRUEBA COMPLETADA ]\n");
                            break;
                        case 7:
                            test_agregarVariasMateriaALista();
                            printf("[ PRUEBA COMPLETADA ]\n");
                            break;
                        case 8:
                            printf("[ VOLVIENDO AL MENU PRINCIPAL... ]\n");
                            break;
                        default:
                            printf("[ OPCION INCORRECTA ]\n");
                            break;
                    }
                } while (opcionPrueba != 8);
            }
                break;
            case 6:
                printf("SALIENDO DEL PROGRAMA...\n");
                break;
            case 7:
                printf("[ OPCION INCORRECTA ]\n");
                break;
        }
    } while (omp!=6);
}