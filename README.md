# Trabajo Practico Grupal

Grupo: HashBoyz

Integrantes:
- Castro, Samuel
- Faillace, Flavio Emanuel
- Freda, Agustín Nicolás
- Cuenca, Matias Alejandro

Docente: Rodriguez, Martin
Materia: Algoritmos y Programacion III / Programación Bajo Nivel
Universidad: Universidad Nacional de Tres de Febrero (UNTREF)
Año: 2023


# Sistema de gestión de estudiantes

Desarrollaremos un sistema de gestión de estudiantes. Un estudiante tendrá:

- Nombre
- Edad
- DNI
- Legajo
- Materias que está cursando
- Materias que aprobó

A su vez este sistema permite la creacion de nuevas materias. Una materia tendra:

- Nombre
- Codigo
- Nota asociada a un alumno


El sistema contará con las siguientes funcionalidades:

## Funcionalidades del Sistema

Ver sección con ejemplo del menú. En resumen:

1. Alta. Un nuevo estudiante o materia se agrega al sistema.
2. Baja. Un estudiante o materia se elimina del sistema.
3. Consulta. Buscar estudiantes o materias por diferentes criterios.
4. Listado de estudiantes o materias.
5. Un estudiante puede darse de alta o baja a una materia.
6. Un estudiante puede rendir una materia.
7. Estadisticas de estudiantes y/o materias.


## Estructuras y funcionamiento

1. Todas los estudiantes/materias estarán guardadas principalmente en una lista.
2. Se deben poder gestionar inscripciones de estudiantes y el establecimiento de las notas.

## Consultas al sistema

1. El sistema debe mostrar una lista de estudiantes/materias registrados en el sistema.
2. El sistema debe permitir buscar al estudiante/materia por una palabra determinada.

---

## Menú Ilustrativo

1. Estudiantes
   1. Agregar Estudiante
      1. Ingresar datos...
   2. Agregar varios alumnos predeterminados
   3. Buscar Estudiante
      1. Por nombre
         1. Ingresar datos...
      2. Por rango de edad
         1. Ingresar datos...
   4. Listar estudiantes registrados
      1. Muestra lista de estudiantes...
   5. Eliminar un estudiante
2. Materias
   1. Agregar Materia
      1. Ingresar datos...
   2. Agregar varias materias predeterminadas
   3. Buscar Materia
      1. Ingresar datos a buscar...
   4. Listar materias registradas
      1. Muestra lista de materias...
   5. Eliminar una materia
3. Inscripciones
   1. Inscribir un alumno a una materia
   2. Rendir una materia
      1. Ingresar nota
      2. Ingresar nota aleatoria
   3. Darse de baja a una materia
4. Estadisticas
   1. Nota promedio de un alumno
   2. Alumno con mejor promedio
   3. Ver materias en curso de un estudiante
   4. Estudiantes inscriptos en una materia

---

# Puntos extras

En este trabajo hemos desarrollado los siguientes ***puntos extras***:

1. Utilización de paginado.
2. Elección de un estudiante/materia en un listado reducido.
3. Generar diversos estudiantes de prueba y materias aleatorias de forma masiva.
4. Estadisticas de los estudiantes/materias.
5. Cálculo de promedios.
6. Tests Unitarios
