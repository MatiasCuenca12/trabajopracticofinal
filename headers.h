#include <stdio.h>
#include <string.h>

typedef struct Materia {
    char nombre[100];
    int codigo;
    float nota;
}Materia;

typedef struct Estudiante {
    char nombre[100];
    int edad;
    long int dni;
    int legajo;
    struct ListaMaterias *aprobadas;
    struct ListaMaterias *cursando;
}Estudiante;

typedef struct NodoMateria {
    Materia materia;
    struct NodoMateria *sgte;
}NodoMateria;

typedef struct NodoEstudiante {
    Estudiante estudiante;
    struct NodoEstudiante *sgte;
}NodoEstudiante;

typedef struct ListaEstudiantes {
    NodoEstudiante *head;
    int cantEstudiantes;
}ListaEstudiantes;

typedef struct ListaMaterias {
    NodoMateria *head;
    int cantMaterias;
}ListaMaterias;



Estudiante crearEstudiante();
int datosCorrectosEstudiante(Estudiante *e);
void mostrarDatosEstudiante(Estudiante e);

Materia crearMateria();
int datosCorrectosMaterias(Materia m);
void mostrarDatosMaterias(Materia m);


ListaEstudiantes crearListaEstudiantes();
void agregarEstudiante(ListaEstudiantes *le, NodoEstudiante **head, Estudiante estudiante);
Estudiante *buscarEstudiantePorNombre(NodoEstudiante **head);
void listarEstudiantes(ListaEstudiantes *le, NodoEstudiante **head);
void listarEstudiantesPaginado(ListaEstudiantes *le, NodoEstudiante **head, int opcion, int Usarpaginado);
void buscarEstudiantesPorRangoDeEdad(NodoEstudiante **head);
void eliminarEstudianteDeLaLista(ListaEstudiantes *le, NodoEstudiante **head);
void anotarseEnMateria(Materia *materia, Estudiante *estudiante);

ListaMaterias crearListaMaterias();
void agregarMateria(ListaMaterias *lm, NodoMateria **chead, Materia materia);
void eliminarMateria(ListaMaterias *lm);
void listarMaterias(ListaMaterias *lm, NodoMateria **head);
void listarMateriasPaginado(ListaMaterias *lm, NodoMateria **head);
int materiaCargada(ListaMaterias *lm, Materia materia);
Materia *buscarMateriaPorNombre(NodoMateria **head);

void asignarNota(ListaMaterias *lm, ListaEstudiantes *le, int aleatorio);
float promedioEstudiante(ListaMaterias *lm);
void *mejorPromedio(ListaEstudiantes le);
void darseBajaMateria(ListaEstudiantes *le);
Estudiante crearEstudianteBis(char nombre[], long int dni, int edad, int legajo);
void agregarVariosEstudiantes(ListaEstudiantes *lista, NodoEstudiante **cabeza);
void agregarVariasMaterias(ListaMaterias *lm, NodoMateria **cabeza);
void verPromedioBuscandoEstudiante(ListaEstudiantes *le);
void listarMateriasEstudiante (ListaEstudiantes *le);
void estudiantesCursandoMateria (ListaMaterias *lm, ListaEstudiantes *le);
void liberarLista(ListaEstudiantes *le);
int estudianteRegistrado(ListaEstudiantes le, Estudiante e);
void test_agregarAlumno();
void test_agregarVariosAlumnos();
void test_buscarAlumnoPorNombre();
void test_eliminarEstudianteDeLaLista();
void test_crearMateria();
void test_agregarMateriaALista();
void test_agregarVariasMateriaALista();

// NUEVO 22/06 02:33
int hayEstudiantesCargados(ListaEstudiantes le);
int hayMateriasCargadas(ListaMaterias lm);