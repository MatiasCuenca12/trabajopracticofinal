#include "headers.h"
#include <stdio.h>
#include "stddef.h"
#include <time.h>
#include <stdlib.h>

/*
 * Verifica si los datos del estudiante son correctos
 */
int datosCorrectosEstudiante(Estudiante *e){
    if(strlen(e->nombre) >= 3 && e->dni > 0 && e->edad >= 17 && e->legajo > 0){
        return 1;
    }
    return 0;
}

/*
 * Imprime los datos de un estudiante
 */
void mostrarDatosEstudiante(Estudiante e){
    printf("  - NOMBRE: %s\n  - DNI: %ld\n  - EDAD: %d\n  - LEGAJO: %d\n", e.nombre, e.dni, e.edad, e.legajo);
}

/*
 * Crea una lista de estudiantes
 */
ListaEstudiantes crearListaEstudiantes() {
    ListaEstudiantes lista;
    lista.head = NULL;
    lista.cantEstudiantes = 0;
    return lista;
}

/*
 * Crea un nodo estudiante
 */
NodoEstudiante *crearNodoEstudiante(Estudiante e){
    NodoEstudiante *nodo = (NodoEstudiante*) malloc(sizeof(NodoEstudiante));
    nodo->estudiante = e;
    nodo->sgte = NULL;
    return nodo;
}

/*
 * Crea un estudiante
 */
Estudiante crearEstudiante(){
    Estudiante estudiante;
    printf("  Ingrese su nombre completo: ");
    scanf(" %[^\n]", estudiante.nombre);
    printf("  Ingrese el DNI: ");
    scanf("%ld", &estudiante.dni);
    printf("  Ingrese la edad: ");
    scanf("%d", &estudiante.edad);
    printf("  Ingrese el legajo: ");
    scanf("%d", &estudiante.legajo);
    estudiante.cursando = (ListaMaterias*) malloc(sizeof(ListaMaterias));
    estudiante.aprobadas = (ListaMaterias*) malloc(sizeof(ListaMaterias));
    *estudiante.cursando = crearListaMaterias();
    *estudiante.aprobadas = crearListaMaterias();
    return estudiante;
}

/*
 * Crea un estudiante pasandole parametros
 */
Estudiante crearEstudianteBis(char nombre[], long int dni, int edad, int legajo){
    Estudiante e;
    strcpy(e.nombre, nombre);
    e.dni = dni;
    e.edad = edad;
    e.legajo = legajo;
    e.cursando = (ListaMaterias*) malloc(sizeof(ListaMaterias));
    e.aprobadas = (ListaMaterias*) malloc(sizeof(ListaMaterias));
    *e.cursando = crearListaMaterias();
    *e.aprobadas = crearListaMaterias();
    return e;
}

/*
 * Agrega un estudiante a la lista
 */
void agregarEstudiante(ListaEstudiantes *le, NodoEstudiante **head, Estudiante e) {
    NodoEstudiante *nuevo = crearNodoEstudiante(e);
    if (*head == NULL) {
        *head = nuevo;
    } else {
        NodoEstudiante *puntero = *head;
        while(puntero->sgte != NULL){
            puntero = puntero -> sgte;
        }
        puntero -> sgte = nuevo;
    }
    le->cantEstudiantes++;
}

/*
 * Imprime el nombre y legajo de cada estudiante utilizando el paginado
 */
void listarEstudiantesPaginado(ListaEstudiantes *le, NodoEstudiante **cabeza, int opcion, int Usarpaginado) {
    static int paginaActual = 1;
    const int tamanoPagina =5;

    int totalEstudiantes = le -> cantEstudiantes;
    int totalPaginas = (totalEstudiantes + tamanoPagina -1) / tamanoPagina;

    int inicio = (paginaActual -1) * tamanoPagina;
    int fin = inicio + tamanoPagina;


    printf("[ ESTUDIANTES CARGADOS: %d ]\n", totalEstudiantes);
    NodoEstudiante *cursor = *cabeza;
    int numero = 1;
    int contador =0;

    while (cursor != NULL) {
        if (contador >= inicio && contador < fin) {
            if (opcion == 1) {
                printf("Estudiante %d:\n", numero);
                mostrarDatosEstudiante(cursor->estudiante);
                numero +=1;
            } else {
                printf("NOMBRE: %s - LEGAJO: %d\n", cursor->estudiante.nombre, cursor->estudiante.legajo);
            }
        }
        cursor = cursor->sgte;
        contador++;
        if (contador >= fin){
            break;
        }

    }
    if (Usarpaginado){
        printf("----#----#----#----#----#----#\n");
        printf("Pagina %d de %d\n", paginaActual, totalPaginas);

        printf("1. Pagina anterior\n");
        printf("2. Pagina siguiente\n");
        printf("3. Volver al menu Estudiantes\n");

        int opcionPaginado;
        printf("Seleccione una opcion: ");
        scanf("%d", &opcionPaginado);

        while (opcionPaginado != 3) {
            switch (opcionPaginado) {
                case 1:
                    if (paginaActual > 1) {
                        paginaActual--;
                    }
                    break;
                case 2:
                    if (paginaActual < totalPaginas) {
                        paginaActual++;
                    }
                    break;
                default:
                    printf("Opcion invalida\n");
                    break;
            }

            inicio = (paginaActual - 1) * tamanoPagina;
            fin = inicio + tamanoPagina;

            contador = 0;
            cursor = *cabeza;
            numero = 1;

            while (cursor != NULL) {
                if (contador >= inicio && contador < fin) {
                    if (opcion == 1) {
                        printf("Estudiante %d:\n", numero);
                        mostrarDatosEstudiante(cursor->estudiante);
                        numero += 1;
                    } else {
                        printf("NOMBRE: %s - LEGAJO: %d\n", cursor->estudiante.nombre, cursor->estudiante.legajo);
                    }
                }

                cursor = cursor->sgte;
                contador++;

                if (contador >= fin) {
                    break;
                }
            }

            printf("----#----#----#----#----#----#\n");
            printf("Pagina %d de %d\n", paginaActual, totalPaginas);

            printf("1. Pagina anterior\n");
            printf("2. Pagina siguiente\n");
            printf("3. Volver al menu Estudiantes\n");

            printf("Seleccione una opcion: ");
            scanf("%d", &opcionPaginado);
        }

        printf("[ VOLVIENDO A MENU ESTUDIANTES... ]\n");
    }
}




/*
 * Busca e imprime los datos de un alumno buscado por su nombre
 */
Estudiante *buscarEstudiantePorNombre(NodoEstudiante **cabeza) {
        char nombre[100];
        printf(">>Ingrese el nombre del estudiante: ");
        scanf(" %[^\n]", nombre);
        NodoEstudiante *cursor = *cabeza;
        Estudiante *estudiante = NULL;

        while (cursor != NULL) {
            if (strcmp(cursor->estudiante.nombre, nombre) == 0){
                estudiante = &cursor->estudiante;
            }
            cursor = cursor -> sgte;
        }
        if (estudiante == NULL) {
            printf("[ EL ESTUDIANTE %s NO EXISTE ]\n", nombre);
        }
        return estudiante;
    }


/*
 * Busca e imprime los datos de alumnos dentro de un rango de edad
 */
void buscarEstudiantesPorRangoDeEdad(NodoEstudiante **cabeza) {

    int inicio,final;

    printf("Ingrese la edad minima: ");
    scanf("%d", &inicio);
    printf("Ingrese la edad maxima: ");
    scanf("%d", &final);

    NodoEstudiante *cursor = *cabeza;
    printf("[ LOS ESTUDIANTES QUE TIENEN ENTRE %d y %d ANOS SON: ]\n", inicio, final);

    while (cursor != NULL) {
        if (cursor-> estudiante.edad >= inicio && cursor->estudiante.edad<= final) {
            printf("NOMBRE: %s - LEGAJO: %d\n", cursor->estudiante.nombre, cursor->estudiante.legajo);
        }
        cursor = cursor->sgte;
    }
}

/*
 * Elimina un estudiante segun el nombre y legajo
 */
void eliminarEstudianteDeLaLista (ListaEstudiantes *le,NodoEstudiante **cabeza) {
        char nombre[100];
        int legajo;
        int eliminado = 0;
        printf(">>Ingrese el nombre completo del estudiante: ");
        scanf(" %[^\n]", nombre);
        printf(">>Ingrese el numero de legajo: ");
        scanf("%d", &legajo);
        NodoEstudiante *aux1 = *cabeza;
        NodoEstudiante *aux2 = NULL;
        while (aux1 != NULL) {
            if (strcmp(aux1->estudiante.nombre, nombre) == 0 && aux1->estudiante.legajo == legajo) {
                if (aux2 != NULL) {
                    aux2->sgte = aux1->sgte;
                } else {
                    *cabeza = aux1->sgte;
                }
                free(aux1);
                eliminado = 1;
                le->cantEstudiantes--;
                break;
            }
            aux2 = aux1;
            aux1 = aux1->sgte;
        }
        if (eliminado == 0) {
            printf("[ EL/LA ESTUDIANTE %s CON LEGAJO %d NO EXISTE ]\n", nombre, legajo);
        } else {
            printf("[ EL/LA ESTUDIANTE %s CON LEGAJO %d FUE ELIMINADO/A ]\n", nombre, legajo);
        }
}

/*
 * Crea una materia
 */
Materia crearMateria() {
    Materia materia;
    printf(">>Ingrese el nombre de la materia: ");
    scanf(" %[^\n]", materia.nombre);
    printf(">>Ingrese el codigo de la materia: ");
    scanf("%d", &materia.codigo);
    materia.nota = 0;
    return materia;
}

/*
 * Crea una materia a traves de parametros
 */
Materia crearMateriaBis(char nombre[], int codigo){
    Materia m;
    strcpy(m.nombre, nombre);
    m.codigo = codigo;
    m.nota = 0;
    return m;
}

/*
 * Crea un nodo materia
 */
NodoMateria *crearNodoMateria(Materia materia){
    NodoMateria *nodo = (NodoMateria*) malloc(sizeof(NodoMateria));
    nodo->materia = materia;
    nodo->sgte = NULL;
    return nodo;
}

/*
 * Crea una lista de materias
 */
ListaMaterias crearListaMaterias() {
    ListaMaterias lista;
    lista.head = NULL;
    lista.cantMaterias = 0;
    return lista;
}

/*
 * Agrega una materia a la lista
 */
void agregarMateria(ListaMaterias *lista, NodoMateria **cabeza, Materia materia) {

    NodoMateria *nuevo = crearNodoMateria(materia);
    if (*cabeza == NULL) {
        *cabeza = nuevo;
    } else {
        NodoMateria *puntero = *cabeza;
        while(puntero->sgte != NULL){
            puntero = (NodoMateria *) puntero->sgte;
        }
        puntero -> sgte = (struct NodoMateria *) nuevo;
    }
    lista->cantMaterias++;
}

/*
 * Elimina una materia d la lista
 */
void eliminarMateria(ListaMaterias *lm) {
    char nombre[100];
    int eliminado = 0;
    printf(">>Ingrese el nombre de la materia: ");
    scanf(" %[^\n]", nombre);

    NodoMateria *aux1 = lm->head;
    NodoMateria *aux2 = NULL;
    while (aux1 != NULL) {
        if (strcmp(aux1->materia.nombre, nombre) == 0) {
            if (aux2 != NULL) {
                aux2->sgte = aux1->sgte;
            } else {
                lm->head = aux1->sgte;
            }
            free(aux1);
            eliminado = 1;
            lm->cantMaterias--;
            break;
        }
        aux2 = aux1;
        aux1 = aux1->sgte;
    }
    if (eliminado == 0) {
        printf("[ LA MATERIA %s NO EXISTE ]\n", nombre);
    } else {
        printf("[ LA MATERIA %s FUE ELIMINADO/A ]\n", nombre);
    }
}


/*
 * Imprime los datos de una materia
 */
void mostrarDatosMaterias(Materia materia){
    printf("- NOMBRE: %s\n  - CODIGO: %d\n ", materia.nombre, materia.codigo);
}

/*
 * Imprime todas las materias cargadas
 */
void listarMateriasPaginado(ListaMaterias *lista, NodoMateria **cabeza) {
    static int pagactual = 1;
    const int tamapag = 4;

    int inicio = (pagactual - 1) * tamapag;
    int fin = inicio + tamapag;
    int contador = 0;
    NodoMateria *cursor = *cabeza;
    int numero = 1;

    while (cursor != NULL && contador < fin) {
        if (contador >= inicio) {
            mostrarDatosMaterias(cursor->materia);
            printf("------------------------\n");
            numero++;
        }
        cursor = cursor->sgte;
        contador++;

        if (contador >= fin) {
            break;
        }
    }

    printf("  --MATERIAS CARGADAS: %d--\n", lista->cantMaterias);
    printf("------------------------\n");

    if (contador > inicio) {
        printf("Pagina %d de %d\n", pagactual, (lista->cantMaterias + tamapag - 1) / tamapag);

        if (pagactual > 1) {
            printf("1. Pagina anterior\n");
        }

        if (pagactual < (lista->cantMaterias + tamapag - 1) / tamapag) {
            printf("2. Pagina siguiente\n");
        }

        printf("3. Volver al menu Materias\n");

        int opcionPaginado;
        printf(">>Seleccione una opcion: ");
        scanf("%d", &opcionPaginado);

        switch (opcionPaginado) {
            case 1:
                if (pagactual > 1) {
                    pagactual--;
                    listarMateriasPaginado(lista, cabeza);
                }
                break;
            case 2:
                if (pagactual < (lista->cantMaterias + tamapag - 1) / tamapag) {
                    pagactual++;
                    listarMateriasPaginado(lista, cabeza);
                }
                break;
            case 3:
                printf("[ VOLVIENDO AL MENU MATERIAS... ]\n");
                break;
            default:
                printf("[ OPCION INVALIDA ]\n");
                break;
        }
    }
}

/*
 * Verifica si una materia ya esta cargada
 */
int materiaCargada(ListaMaterias *lm, Materia materia){
   NodoMateria *cursor = lm->head;
   while(cursor != NULL ){
       if(strcmp(cursor->materia.nombre, materia.nombre) == 0){
           return 1;
       }
       cursor = cursor->sgte;
   }
   return 0;
}

/*
 * Verifica si los datos ingresados son correctos
 */
int datosCorrectosMaterias(Materia materia){
    if(strlen(materia.nombre) >= 3 && materia.codigo > 0){
        return 1;
    }
    return 0;
}

/*
 * Busca una materia por nombre y la retorna
 */
Materia *buscarMateriaPorNombre(NodoMateria **head) {
        char nombre[100];
        printf(">>Ingrese el nombre de la materia: ");
        scanf(" %[^\n]", nombre);
        NodoMateria* cursor = *head;
        Materia *materia = NULL;

        while (cursor != NULL) {
            if (strcmp(cursor->materia.nombre, nombre) == 0) {
                materia = &cursor->materia;
                break;
            }
            cursor = cursor->sgte;
        }
        if (materia == NULL) {
            printf("[ LA MATERIA %s NO EXISTE ]\n", nombre);
        }
        return materia;
    }

/*
 * Verifica si una nota esta entre 0 y 10
 */
int notaCorrecta(float nota) {
    if (nota > 0 & nota <= 10) {
        return 1;
    }
    return 0;
}

/*
 * Asigna nota a una materia
 */
void asignarNota(ListaMaterias *lm, ListaEstudiantes *le, int opcion) {
    float nota;
    Materia *materia;
    Estudiante *estudiante;
    listarEstudiantes(le, &le->head);
    estudiante = buscarEstudiantePorNombre(&le->head);
    if (estudiante != NULL & estudiante->cursando->head != NULL) {
        printf("[ MATERIAS QUE ESTA CURSANDO %s ]\n", estudiante->nombre);
        listarMaterias(estudiante->cursando,&estudiante->cursando->head);
        materia = buscarMateriaPorNombre(&estudiante->cursando->head);

        if (opcion == 1) {
            printf(">>Ingrese la nota final de cursada: ");
            scanf("%f",&nota);
            if(notaCorrecta(nota)){
                printf("[ LA NOTA FINAL ES: %.2f ]\n", nota);
                materia->nota= (float)nota;
            }
        } else {
            srand(time(NULL));
            nota =(float)(rand() % 10)+1;
            printf("[ LA NOTA DEL EXAMEN ES: %.2f ]\n", nota);
            materia->nota= nota;
        }
    }
    else {
        printf("[ %s NO ESTA CURSANDO MATERIAS ]\n", estudiante->nombre);
    }
}

/*
 * Anota a un estudiante a una materia de la lista
 */
void anotarseEnMateria(Materia *materia, Estudiante *estudiante) {
    NodoMateria *nuevo = crearNodoMateria(*materia);
    if (estudiante->cursando->head == NULL) {
        estudiante->cursando->head = nuevo;
    } else {
        NodoMateria *puntero = estudiante->cursando->head;
        while (puntero->sgte != NULL) {
            puntero = puntero->sgte;
        }
        puntero->sgte = nuevo;
    }
    estudiante->cursando->cantMaterias++;
}

/*
 * Se da de baja a un alumno en una materia
 */
void darseBajaMateria(ListaEstudiantes *le) {
    Estudiante *estudiante = buscarEstudiantePorNombre(&le->head);
    eliminarMateria(estudiante->cursando);
}

/*
 * Se obtiene el alumno con mejor promedio
 */
void *mejorPromedio(ListaEstudiantes le) {
    float mejorPromedio = 0, promedio = 0;
    Estudiante *estudianteMejorPromedio = NULL;
    NodoEstudiante *punteroEstudiante = le.head;
    while (punteroEstudiante != NULL) {
        promedio = promedioEstudiante(punteroEstudiante->estudiante.cursando);
        if((promedio != 0) & (mejorPromedio == 0 | promedio > mejorPromedio)) {

            estudianteMejorPromedio = &(punteroEstudiante->estudiante);
            mejorPromedio = promedio;
        }
        punteroEstudiante = punteroEstudiante->sgte;
    }
    if (mejorPromedio != 0) {
        printf("[ EL ESTUDIANTE CON EL MEJOR PROMEDIO %.2f ES %s ]\n", mejorPromedio, estudianteMejorPromedio->nombre);
        //printf("Nombre: %s  Promedio: %f\n",estudianteMejorPromedio->nombre, mejorPromedio);
    } else {
        printf("[ LOS ESTUDIANTES NO TIENEN NOTAS ]\n");
    }
    return NULL;
}

/*
 * Lista las materias que esta cursando un estudiante
 */
void listarMateriasEstudiante (ListaEstudiantes *le) {
    Estudiante *estudiante = buscarEstudiantePorNombre(&le->head);
    printf("[ MATERIAS QUE ESTA CURSANDO %s: ]\n", estudiante->nombre);
    listarMaterias(estudiante->cursando,&estudiante->cursando->head);
}

/*
 * Mestra los estudiantes que estan cursando una materia
 */
void estudiantesCursandoMateria(ListaMaterias *lm, ListaEstudiantes *le) {
    ListaEstudiantes *inscriptos = (ListaEstudiantes*) malloc(sizeof(ListaEstudiantes));
    *inscriptos = crearListaEstudiantes();
    Materia *materia = buscarMateriaPorNombre(&lm->head);
    int cantidad = 0;
    NodoEstudiante *punteroEstudiante = le->head;

    while (punteroEstudiante != NULL) {
        NodoMateria *punteroMateria = punteroEstudiante->estudiante.cursando->head;
        while (punteroMateria != NULL) {
            if(strcmp(punteroMateria->materia.nombre, materia->nombre) == 0) {
                agregarEstudiante(inscriptos, &inscriptos->head, punteroEstudiante->estudiante);
                cantidad += 1;
            }
            punteroMateria = punteroMateria->sgte;
        }
        punteroEstudiante = punteroEstudiante->sgte;
    }

    printf("[ LISTA DE ESTUDIANTE CURSANDO %s ]\n", materia->nombre);
    listarEstudiantes(inscriptos, &inscriptos->head);
    //listarEstudiantesPaginado(inscriptos, &inscriptos->head, 0, 0);
    printf("[ LA MATERIA TIENE %d ESTUDIANTES ANOTADOS ]\n", cantidad);
    liberarLista(inscriptos);
}

void liberarLista(ListaEstudiantes *le) {
    NodoEstudiante *actual = le->head;
    while (actual != NULL) {
        NodoEstudiante *siguiente = actual->sgte;
        free(actual);
        actual = siguiente;
    }
    le->head = NULL;
}

/*
 * Muestra la nota promedio de un estudiante
 */
void verPromedioBuscandoEstudiante(ListaEstudiantes *le) {
    Estudiante *estudiante = (buscarEstudiantePorNombre(&le->head));
    float nota = promedioEstudiante(estudiante->cursando);
    if (nota!=0) {
        printf("[ LA NOTA PROMEDIO DE %s ES %.2f ]\n", estudiante->nombre, nota);
    }
    else {
        printf("[ %s NO TIENE NOTAS CARGADAS ]\n", estudiante->nombre);
    }
}

/*
 * Se obtiene un promedio de notas
 */
float promedioEstudiante(ListaMaterias *lm) {
    float suma =0, promedio = 0;
    int cantidad = 0;
    NodoMateria *puntero = lm->head;
    while (puntero != NULL) {
        if (puntero->materia.nota!=0) {
            suma += puntero->materia.nota;
            cantidad +=1;
        }
        puntero = puntero->sgte;
    }
    if (lm->cantMaterias != 0 & suma != 0) {
        promedio = suma / (float) cantidad;
    }
    return promedio;
}

/*
 * Agrega un lote de estudiantes a la lista
 */
void agregarVariosEstudiantes(ListaEstudiantes *le, NodoEstudiante **head){
    Estudiante estudiantes[11];
    Materia materias[11];
    int cantEstudiantes = sizeof(estudiantes)/sizeof(Estudiante);

    estudiantes[0] = crearEstudianteBis("Novak Djokovic", 32005543, 25, 2423);
    estudiantes[1] = crearEstudianteBis("Rafael Nadal", 33235597, 22,2434);
    estudiantes[2] = crearEstudianteBis("Serena Williams", 31005543, 35, 2445);
    estudiantes[3] = crearEstudianteBis("Maria Sharapova", 30005543, 24, 2456);
    estudiantes[4] = crearEstudianteBis("Mario Balotelli", 34005543, 23, 2467);
    estudiantes[5] = crearEstudianteBis("Cristiano Ronaldo", 35005543, 20, 2489);
    estudiantes[6] = crearEstudianteBis("Zinadine Zidane", 36005543, 30, 2490);
    estudiantes[7] = crearEstudianteBis("Juan Roman Riquelme", 37005543, 32, 2500);
    estudiantes[8] = crearEstudianteBis("Andrea Pirlo", 38005543, 35, 2512);
    estudiantes[9] = crearEstudianteBis("Lionel Messi", 39005543, 27, 1010);
    estudiantes[10] = crearEstudianteBis("Pele",40488107,25,45539);


    materias[0] = crearMateriaBis("Analisis matematico I", 1001);
    materias[1] = crearMateriaBis("Analisis matematico II", 1002);
    materias[2] = crearMateriaBis("Fisica I", 1003);
    materias[3] = crearMateriaBis("Cultura contemporanea", 1004);
    materias[4] = crearMateriaBis("Algebra I", 1005);
    materias[5] = crearMateriaBis("Ingles I", 1006);
    materias[6] = crearMateriaBis("Base de datos", 1007);
    materias[7] = crearMateriaBis("Algoritmos y programacion I", 1008);
    materias[8] = crearMateriaBis("Diseno Logico", 1009);
    materias[9] = crearMateriaBis("Sistemas operativos", 1010);
    materias[10] = crearMateriaBis("Electronica", 1011);
    srand(time(0));
    int random;

    for(int i=0;i<cantEstudiantes;i++){
        agregarEstudiante(le, head, estudiantes[i]);
        for(int j=0; j<3 ; j++){
            random = rand()%11;
            anotarseEnMateria(&materias[random],&estudiantes[i]);
        }
    }
    printf("[ ESTUDIANTES AGREGADOS CON EXITO ]\n");
}

/*
 * Carga un lote de materias predefinidas
 */
void agregarVariasMaterias(ListaMaterias *lm, NodoMateria **cabeza){

    Materia materias[10];
    int cantMaterias = sizeof(materias)/sizeof(Materia);

    materias[0] = crearMateriaBis("Analisis matematico I", 1001);
    materias[1] = crearMateriaBis("Analisis matematico II", 1002);
    materias[2] = crearMateriaBis("Fisica I", 1003);
    materias[3] = crearMateriaBis("Cultura contemporanea", 1004);
    materias[4] = crearMateriaBis("Algebra I", 1005);
    materias[5] = crearMateriaBis("Ingles I", 1006);
    materias[6] = crearMateriaBis("Base de datos", 1007);
    materias[7] = crearMateriaBis("Algoritmos y programacion I", 1008);
    materias[8] = crearMateriaBis("Diseno Logico", 1009);
    materias[9] = crearMateriaBis("Sistemas operativos", 1010);

    for(int i=0; i<cantMaterias; i++){
        agregarMateria(lm, cabeza, materias[i]);
    }
    printf("[ MATERIAS CARGADAS CON EXITO ]\n");
}

/*
 * Verifica si ya hay un estudiante registrado por su DNI o legajo
 */
int estudianteRegistrado(ListaEstudiantes le, Estudiante e){
    NodoEstudiante  *puntero = le.head;
    if(puntero == NULL){
        return 0;
    }else{
        while(puntero != NULL){
            Estudiante a = puntero->estudiante;
            if(a.dni == e.dni || a.legajo == e.legajo){
                return 1;
            }
            puntero = puntero->sgte;
        }
    }
    return 0;
}

/*
 * Verifica si hay estudiantes cargados
 */
int hayEstudiantesCargados(ListaEstudiantes le){
    if(le.cantEstudiantes == 0){
        return 0;
    }
    return 1;
}

/*
 * Verifica si hay materias cargadas
 */
int hayMateriasCargadas(ListaMaterias lm){
    if(lm.cantMaterias == 0){
        return 0;
    }
    return 1;
}

/*
 * Lista a todos los estudiantes de una sola vez
 */
void listarEstudiantes(ListaEstudiantes *le, NodoEstudiante **head){
    NodoEstudiante *cursor = *head;
    printf("[ ESTUDIANTES INSCRIPTOS: %d ]\n", le->cantEstudiantes);
    while(cursor != NULL){
        printf("NOMBRE: %s - LEGAJO: %d\n", cursor->estudiante.nombre, cursor->estudiante.legajo);
        cursor = cursor->sgte;
    }
}

/*
 * Lista todas las materias de una sola vez
 */
void listarMaterias(ListaMaterias *lm, NodoMateria **head){
    NodoMateria *cursor = *head;
    //printf("[ MATERIAS DISPONIBLES: %d ]\n", lm->cantMaterias);
    while(cursor != NULL){
        printf("NOMBRE: %s - CODIGO: %d\n", cursor->materia.nombre, cursor->materia.codigo);
        cursor = cursor->sgte;
    }
}

// TEST 1
void test_agregarAlumno(){
    // lista estudiantes vacia
    ListaEstudiantes lista = crearListaEstudiantes();
    NodoEstudiante *cabeza = NULL;

    // estudiante
    Estudiante estudiante1 = crearEstudianteBis("Juan Perez", 12345678, 20, 1001);

    // agregar estudiante a la lista
    agregarEstudiante(&lista, &lista.head, estudiante1);

    // ver si la cantidad de alumnos en la lista es 1
    if (lista.cantEstudiantes != 1) {
        printf("Error en el Test 1: La cantidad de estudiantes debería ser 1, pero es %d\n", lista.cantEstudiantes);
    } else {
        printf("Test 1: PASS\n");
    }
}

// TEST 2
void test_agregarVariosAlumnos(){
    // lista estudiantes vacia
    ListaEstudiantes lista = crearListaEstudiantes();
    NodoEstudiante *cabeza = NULL;

    // estudiantes de prueba
    Estudiante estudiante1 = crearEstudianteBis("Juan Perez", 12345678, 20, 1001);
    Estudiante estudiante2 = crearEstudianteBis("Maria Rodriguez", 23456789, 21, 1002);
    Estudiante estudiante3 = crearEstudianteBis("Pedro Martinez", 34567890, 22, 1003);

    // estudiantes a la lista
    agregarEstudiante(&lista, &lista.head, estudiante1);
    agregarEstudiante(&lista, &lista.head, estudiante2);
    agregarEstudiante(&lista, &lista.head, estudiante3);

    // cantidad de estudiantes en la lista sea 3
    if (lista.cantEstudiantes != 3) {
        printf("Error en el Test 2: La cantidad de estudiantes debería ser 3, pero es %d\n", lista.cantEstudiantes);
    } else {
        listarEstudiantesPaginado(&lista, &cabeza, 2, 0);
        printf("Test 2: PASS\n");
    }
}

// TEST 3
void test_buscarAlumnoPorNombre(){
    // lista de estudiantes con algunos estudiantes
    ListaEstudiantes lista = crearListaEstudiantes();
    NodoEstudiante *cabeza = NULL;

    Estudiante estudiante1 = crearEstudianteBis("Juan Perez", 12345678, 20, 1001);
    Estudiante estudiante2 = crearEstudianteBis("Maria Rodriguez", 23456789, 21, 1002);
    Estudiante estudiante3 = crearEstudianteBis("Pedro Martinez", 34567890, 22, 1003);

    agregarEstudiante(&lista, &lista.head, estudiante1);
    agregarEstudiante(&lista, &lista.head, estudiante2);
    agregarEstudiante(&lista, &lista.head, estudiante3);

    // buscar el estudiante "Maria Rodriguez"
    Estudiante *estudianteBuscado = buscarEstudiantePorNombre(&cabeza);

    // ver que el estudiante encontrado sea correcto
    if (estudianteBuscado == NULL || strcmp(estudianteBuscado->nombre, "Maria Rodriguez") != 0) {
        printf("Error en el Test 3: No se encontro correctamente el estudiante\n");
    } else {
        printf("Test 3: PASS\n");
    }
}

// TEST 4
void test_eliminarEstudianteDeLaLista(){
    // lista de estudiantes con algunos estudiantes
    ListaEstudiantes lista = crearListaEstudiantes();
    NodoEstudiante *cabeza = NULL;

    Estudiante estudiante1 = crearEstudianteBis("Juan Perez", 12345678, 20, 1001);
    Estudiante estudiante2 = crearEstudianteBis("Maria Rodriguez", 23456789, 21, 1002);
    Estudiante estudiante3 = crearEstudianteBis("Pedro Martinez", 34567890, 22, 1003);

    agregarEstudiante(&lista, &lista.head, estudiante1);
    agregarEstudiante(&lista, &lista.head, estudiante2);
    agregarEstudiante(&lista, &lista.head, estudiante3);

    // eliminar el estudiante "Maria Rodriguez" con legajo 1002
    eliminarEstudianteDeLaLista(&lista, &cabeza);

    // ver que la cantidad de estudiantes en la lista sea 2
    if (lista.cantEstudiantes != 2) {
        printf("Error en el Test 3: La cantidad de estudiantes debería ser 2, pero es %d\n", lista.cantEstudiantes);
    } else {
        listarEstudiantesPaginado(&lista, &cabeza, 2, 0);
        printf("Test 4: PASS\n");
    }
}

// TEST 5
void test_crearMateria(){
    // nueva materia
    Materia materia = crearMateria();

    // ver que los datos ingresados sean correctos
    if (strcmp(materia.nombre, "Matematicas") != 0 || materia.codigo != 101 || materia.nota != 0) {
        printf("Error en el Test 5: Los datos ingresados no son correctos\n");
    } else {
        printf("Test 5: PASS\n");
    }

}

// TEST 6
void test_agregarMateriaALista(){
    // lista de materias vacia
    ListaMaterias lista = crearListaMaterias();
    NodoMateria *cabeza = NULL;

    // se crea un materia y se agrega a la lista
    Materia materia1 = crearMateriaBis("Matematicas", 101);
    agregarMateria(&lista, &cabeza, materia1);

    // cantidad de materias en la lista sea 1
    if (lista.cantMaterias != 1) {
        printf("Error en el Test 6: La cantidad de materias debería ser 1, pero es %d\n", lista.cantMaterias);
    } else {
        listarMateriasPaginado(&lista, &cabeza);
        printf("Test 6: PASS\n");
    }
}

// TEST 7
void test_agregarVariasMateriaALista(){
    // lista de materias vacia
    ListaMaterias lista = crearListaMaterias();
    NodoMateria *cabeza = NULL;

    // varias materias y se agregan a la lista
    Materia materia1 = crearMateriaBis("Matematicas I", 101);
    Materia materia2 = crearMateriaBis("Matematicas II", 102);
    Materia materia3 = crearMateriaBis("Matematicas III", 103);
    Materia materia4 = crearMateriaBis("Matematicas IV", 104);
    agregarMateria(&lista, &cabeza, materia1);
    agregarMateria(&lista, &cabeza, materia2);
    agregarMateria(&lista, &cabeza, materia3);
    agregarMateria(&lista, &cabeza, materia4);

    // cantidad de materias en la lista sea 4
    if (lista.cantMaterias != 4) {
        printf("Error en el Test 7: La cantidad de materias debería ser 4, pero es %d\n", lista.cantMaterias);
    } else {
        listarMateriasPaginado(&lista, &cabeza);
        printf("Test 7: PASS\n");
    }
}

